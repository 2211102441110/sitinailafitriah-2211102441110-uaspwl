from rest_framework import serializers
from berita.models import Kategori, Artikel
from pengguna.models import Biodata

class BiodataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Biodata
        fields = '__all__'

class KategoriSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kategori
        fields = '__all__'

class ArtikelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artikel
        fields = '__all__'