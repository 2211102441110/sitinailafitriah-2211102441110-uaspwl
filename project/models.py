from django.db import models
from django.contrib.auth.models import User

# Create your models here.
# One-to-One (Satu ke Satu)
# Contoh: Setiap pelanggan dapat memiliki satu akun pengguna (user account).
class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=15)
    address = models.CharField(max_length=255)

    def __str__(self):
        return self.user.username

# One-to-Many (Satu ke Banyak)
# Contoh: Setiap Coffeeshop memiliki banyak produk yang dijual
from django.db import models

class Coffeeshop(models.Model):
    name = models.CharField(max_length=255)
    location = models.CharField(max_length=255)

    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "2. Coffeeshop"

class Product(models.Model):
    coffeeshop = models.ForeignKey(Coffeeshop, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    description = models.TextField()

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "4. Product"


# Many-to-Many (Banyak ke Banyak)
# Contoh: Pelanggan dapat memiliki beberapa preferensi rasa kopi, dan setiap rasa kopi dapat disukai oleh banyak pelanggan.
from django.db import models

class Customer(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField()

    def __str__(self):
        return self.name

class CoffeeFlavor(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "1. CoffeeFlavor"
class CustomerPreferences(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    flavor = models.ManyToManyField(CoffeeFlavor)

    def __str__(self):
        return self.customer.name + "'s Preferences"

    class Meta:
        verbose_name_plural = "3. CustomerPreferences"
