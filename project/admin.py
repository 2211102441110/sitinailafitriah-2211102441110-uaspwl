from django.contrib import admin
from .models import Customer, Coffeeshop, Product, CoffeeFlavor, CustomerPreferences

# Register your models here.

admin.site.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('name', 'email')
    search_fields = ('name', 'email')

admin.site.register(Coffeeshop)
class CoffeeshopAdmin(admin.ModelAdmin):
    list_display = ('name', 'location')
    search_fields = ('name', 'location')

admin.site.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'coffeeshop', 'price')
    list_filter = ('coffeeshop__name', 'price')
    search_fields = ('name', 'coffeeshop__name')

admin.site.register(CoffeeFlavor)
class CoffeeFlavorAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)

admin.site.register(CustomerPreferences)
class CustomerPreferencesAdmin(admin.ModelAdmin):
    list_display = ('customer', 'flavors_list')
    search_fields = ('customer__name',)

    def flavors_list(self, obj):
        return ", ".join([flavor.name for flavor in obj.flavor.all()])
